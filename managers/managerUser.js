const { UniqueConstraintError } = require('sequelize/dist');
const db = require('../db');
const { User } = db.models;
var jwt = require('jsonwebtoken');
const { Console } = require('console');

async function createUser(newUser){
 
    const existingUsers = await User.findAll({
        where: {
            email: newUser.email,
            
        }
      });
      
     if (existingUsers.length >0){
        throw new Error("user duplicate")
     }

    let user = await User.create({
        email: newUser.email,
        password: newUser.password
        
    });
    

    return user;
}

async function loginUser(email, password){

    const existingUsers = await User.findAll({
        where: {
            email: email,
        }
      });

    if(existingUsers.length === 0 || existingUsers[0].password !== password)
      throw Error("credential not valid");
      
    const token = await jwt.sign({
        id: existingUsers[0].id,
        email: existingUsers[0].email,
    }, '_secret_');

    return token;
}

async function updateUser(id, editedUser){
    
    const existingUser = await User.findByPk(id);

    if(!existingUser)
        throw new Error("user doesn't exist");

    await existingUser.update(editedUser);
    console.log("existing user",existingUser);
}

async function getUserById (id){
    const existingUser = await User.findByPk(id);
    return existingUser;

}

async function getAllUser(){

    const existingUser = await User.findAll();
    return existingUser;

}

async function deleteUser(id){
    const existingUser = await User.findByPk(id);
    await existingUser.destroy();
    
}



module.exports = {
    createUser,
    updateUser,
    getUserById,
    getAllUser,
    deleteUser,
    loginUser
};