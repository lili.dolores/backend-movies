const db = require('./../db');
const { Actor } = db.models;

async function createActor(newActor){

    const existingActors = await Actor.findAll({
        where: {
            fullName: newActor.fullName,
            
        }
      });
      
     if (existingActors.length >0){
        throw new Error("actor duplicate")
     }

    let actor = await Actor.create({
        fullName: newActor.fullName,
        dateOfBirth: newActor.dateOfBirth
    });
    

    return actor;
}

async function updateActor(id, editedActor){
    
    const existingActor = await Actor.findByPk(id);

    if(!existingActor)
        throw new Error("Actor doesn't exist");

    await existingActor.update(editedActor);
    console.log("existing actor",existingActor);
}

async function getActorById (id){
    const existingActor = await Actor.findByPk(id);
    return existingActor;

}

async function getAllActors(){

    const existingActors = await Actor.findAll();
    return existingActors;

}

async function deleteActor(id){
    const existingActor = await Actor.findByPk(id);
    await existingActor.destroy();
    
}

module.exports = {
    createActor,
    updateActor,
    getActorById,
    getAllActors,
    deleteActor
};