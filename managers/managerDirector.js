const db = require('../db');
const { Director } = db.models;

async function createDirector(newDirector){

    const existingDirectors = await Director.findAll({
        where: {
            fullName: newDirector.fullName,
            
        }
      });
      
     if (existingDirectors.length >0){
        throw new Error("director duplicate")
     }

    let director = await Director.create({
        fullName: newDirector.fullName,
        dateOfBirth: newDirector.dateOfBirth
    });
    

    return director;
}

async function updateDirector(id , editDirector){
    const existingDirector = await Director.findByPk(id)

    if(!existingDirector)
        throw new Error ("Director doesn't exist");
    
    await existingDirector.update(editDirector)

};

async function getDirectorById (id){
    const existingDirector = await Director.findByPk(id);
    return existingDirector;

}

async function getAllDirector(){

    const existingDirector = await Director.findAll();
    return existingDirector;

}

async function deleteDirector(id){
    const existingDirector = await Director.findByPk(id);
    await existingDirector.destroy();
    
}






module.exports = {
    createDirector,
    updateDirector,
    getDirectorById,
    getAllDirector,
    deleteDirector

    
};