const db = require('./../db');
const { Gender } = db.models;

async function createGender(newGender){

    const existingGenders = await Gender.findAll({
        where: {
            description: newGender.description,
            
        }
      });
      
     if (existingGenders.length >0){
        throw new Error("gender duplicate")
     }

    let gender = await Gender.create({
        description: newGender.description,
        
    });
    

    return gender;
}

async function updateGender(id, editedGender){
    
    const existingGender = await Gender.findByPk(id);

    if(!existingGender)
        throw new Error("gender doesn't exist");

    await existingGender.update(editedGender);
    console.log("existing gender",existingGender);
}

async function getGenderById (id){
    const existingGender = await Gender.findByPk(id);
    return existingGender;

}

async function getAllGender(){

    const existingGender = await Gender.findAll();
    return existingGender;

}

async function deleteGender(id){
    const existingGender = await Gender.findByPk(id);
    await existingGender.destroy();
    
}

module.exports = {
    createGender,
    updateGender,
    getGenderById,
    getAllGender,
    deleteGender
};