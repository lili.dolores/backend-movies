const { Sequelize, Op } = require('sequelize/dist');
const { ne } = require('sequelize/dist/lib/operators');
const db = require('../db');
const { Movie } = db.models;

async function createMovie(newMovie){

    const existingMovies = await Movie.findAll({
        where: {
            
            [Op.or]: [
                {
                  title: newMovie.title,
                },
                {
                  codigo: newMovie.codigo
                }
              ]
        }
    });
    console.log("lili");
     if (existingMovies.length >0){
        throw new Error("movie duplicate")
     }

    let movie = await Movie.create({
        codigo: newMovie.codigo,
        title: newMovie.title,
        year: newMovie.year,
        description: newMovie.description,
        director_id: newMovie.director_id,
        gender_id: newMovie.gender_id,
        stars: newMovie.stars, 
        photo_url: newMovie.photo_url
    });
    

    return movie;
}

async function updateMovie(id, editedMovie){
    
    const existingMovie = await Movie.findByPk(id);

    if(!existingMovie)
        throw new Error("Movie doesn't exist");

    await existingMovie.update(editedMovie);
    console.log("existing movie",existingMovie);
}

async function getMovieById (id){
    const existingMovie = await Movie.findByPk(id);
    return existingMovie;

}

async function getAllMovies(){

    const existingMovie = await Movie.findAll();
    return existingMovie;

}

async function deleteMovie(id){
    const existingMovie = await Movie.findByPk(id);
    await existingMovie.destroy();
    
}

module.exports = {
    createMovie,
    updateMovie,
    getAllMovies,
    getMovieById,
    deleteMovie
};