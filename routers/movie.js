const express = require('express');
const router = express.Router();
const yup = require('yup');
const validateSchema = require('./schemaValidation');
const MovieManager = require('../managers/managerMovie');

const createMovieSchema = yup.object({
      body: yup.object().shape({
        codigo: yup.number().required(),
        title: yup.string().required(),
        year: yup.date().required(),
        description: yup.string().required(),
        stars: yup.number().required(),
        photo_url: yup.string().required(),
      }),
    });

const editMovieSchema = yup.object({
  body: yup.object().shape({
    codigo: yup.number().required(),
    title: yup.string().required(),
    year: yup.date().required(),
    description: yup.string().required(),
    stars: yup.number().required(),
    photo_url: yup.string().required(),
  }),
});


router.post('/', validateSchema(createMovieSchema), (async (req, res) => {
     try {
      
      let movie = await MovieManager.createMovie(req.body);
      res.send(movie);

     } catch (error) {
       console.error(`Error while creating a new movie ${error.message}`);
       res.status(500).send({error: error.message});
     }
}));


router.put('/:id', validateSchema(editMovieSchema), (async (req, res) => {

  let movieEdit = await MovieManager.updateMovie(req.params.id, req.body) 
  res.send(movieEdit); 

}));

router.get('/', (async (req, res) => {

  let movie = await MovieManager.getAllMovies() 
  res.send(movie); 

}));

router.get('/:id', (async (req, res) => {

  let movie = await MovieManager.getMovieById(req.params.id) 
  res.send(movie); 

}));

router.delete('/:id', async (req,res) => {
  let movie = await MovieManager.deleteMovie(req.params.id)
  res.send();
})




module.exports = router;