const validateSchema = (schema) => async (req, res, next) => {
    try {
      await schema.validate({
        body: req.body,
      });
      console.log("VALIDATION OK!")
      return next();
    } catch (err) {
      console.log(">>> VALIDATION NOT OK")
      return res.status(500).json({ type: err.name, message: err.message });
    }
  };

  module.exports = validateSchema;