const express = require('express');
const router = express.Router();
const yup = require('yup');
const validateSchema = require('./schemaValidation');
const DirectorManager = require('../managers/managerDirector');

const createDirectorSchema = yup.object({
      body: yup.object().shape({
        fullName: yup.string().required(),
        dateOfBirth: yup.date().required(),
      }),
    });

const editDirectorSchema = yup.object({
  body: yup.object().shape({
    fullName: yup.string().required(),
    dateOfBirth: yup.date().required(),
  }),
});


router.post('/', validateSchema(createDirectorSchema), (async (req, res) => {
     try {
      
      let director = await DirectorManager.createDirector(req.body);
      res.send(director);

     } catch (error) {
       console.error(`Error while creating a new director ${error.message}`);
       res.status(500).send({error: error.message});
     }
}));


router.put('/:id', validateSchema(editDirectorSchema), (async (req, res) => {

  let directorEdit = await DirectorManager.updateDirector(req.params.id, req.body) 
  res.send(directorEdit); 

}));

router.get('/', (async (req, res) => {

  let director = await DirectorManager.getAllDirector() 
  res.send(director); 

}));

router.get('/:id', (async (req, res) => {

  let director = await DirectorManager.getDirectorById(req.params.id) 
  res.send(director); 

}));

router.delete('/:id', async (req,res) => {
  let director = await DirectorManager.deleteDirector(req.params.id)
  res.send();
})




module.exports = router;