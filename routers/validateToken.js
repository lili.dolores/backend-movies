var jwt = require('jsonwebtoken');

const validateToken = async (req, res, next) => {
    try {
      if(req.originalUrl === "/user/getToken")
        return next();

      const token = req.headers.token;
      
      const decodedUser = jwt.verify(token, '_secret_');
      req.headers.user = decodedUser;
      if(!decodedUser){
        throw Error();
      }
      console.log("TOKEN OK!", decodedUser)
      return next();
    } catch (err) {
      console.log(">>> TOKEN NOT OK")
      return res.status(500).json({ type: err.name, message: err.message });
    }
  };

  module.exports = validateToken;