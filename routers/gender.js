const express = require('express');
const router = express.Router();
const yup = require('yup');
const validateSchema = require('./schemaValidation');
const GenderManager = require('../managers/managerGender');

const createGenderSchema = yup.object({
      body: yup.object().shape({
        description: yup.string().required(),
      }),
    });

const editGenderSchema = yup.object({
  body: yup.object().shape({
    description: yup.string().required(),
  }),
});


router.post('/', validateSchema(createGenderSchema), (async (req, res) => {
     try {
      
      let gender = await GenderManager.createGender(req.body);
      res.send(gender);

     } catch (error) {
       console.error(`Error while creating a new gender ${error.message}`);
       res.status(500).send({error: error.message});
     }
}));


router.put('/:id', validateSchema(editGenderSchema), (async (req, res) => {

  let genderEdit = await GenderManager.updateGender(req.params.id, req.body) 
  res.send(genderEdit); 

}));

router.get('/', (async (req, res) => {

  let gender = await GenderManager.getAllGender() 
  res.send(gender); 

}));

router.get('/:id', (async (req, res) => {

  let gender = await GenderManager.getGenderById(req.params.id) 
  res.send(gender); 

}));

router.delete('/:id', async (req,res) => {
  let gender = await GenderManager.deleteGender(req.params.id)
  res.send();
})




module.exports = router;