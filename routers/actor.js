const express = require('express');
const router = express.Router();
const yup = require('yup');
const validateSchema = require('./schemaValidation');
const ActorManager = require('../managers/managerActor');

const createActorSchema = yup.object({
      body: yup.object().shape({
        fullName: yup.string().required(),
        dateOfBirth: yup.date().required(),
      }),
    });

const editActorSchema = yup.object({
  body: yup.object().shape({
    fullName: yup.string().required(),
    dateOfBirth: yup.date().required(),
  }),
});


router.post('/', validateSchema(createActorSchema), (async (req, res) => {
     try {
      
      let actor = await ActorManager.createActor(req.body);
      res.send(actor);

     } catch (error) {
       console.error(`Error while creating a new actor ${error.message}`);
       res.status(500).send({error: error.message});
     }
}));


router.put('/:id', validateSchema(editActorSchema), (async (req, res) => {

  let actorEdit = await ActorManager.updateActor(req.params.id, req.body) 
  res.send(actorEdit); 

}));

router.get('/', (async (req, res) => {

  let actor = await ActorManager.getAllActors() 
  res.send(actor); 

}));

router.get('/:id', (async (req, res) => {

  let actor = await ActorManager.getActorById(req.params.id) 
  res.send(actor); 

}));

router.delete('/:id', async (req,res) => {
  let actor = await ActorManager.deleteActor(req.params.id)
  res.send();
})




module.exports = router;