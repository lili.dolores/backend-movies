const express = require('express');
const router = express.Router();
const yup = require('yup');
const validateSchema = require('./schemaValidation');
const UserManager = require('../managers/managerUser');

const createUserSchema = yup.object({
      body: yup.object().shape({
        email: yup.string().required(),
      }),
    });

const editUserSchema = yup.object({
  body: yup.object().shape({
    email: yup.string().required(),
}),
});

const loginUserSchema = yup.object({
    body: yup.object().shape({
      email: yup.string().required(),
      password: yup.string().required(),
  }),
});

router.post('/', validateSchema(createUserSchema), (async (req, res) => {
     try {
      
      let user = await UserManager.createUser(req.body);
      res.send(user);

     } catch (error) {
       console.error(`Error while creating a new user ${error.message}`);
       res.status(500).send({error: error.message});
     }
}));


router.put('/:id', validateSchema(editUserSchema), (async (req, res) => {

  let userEdit = await UserManager.updateUser(req.params.id, req.body) 
  res.send(userEdit); 

}));

router.get('/', (async (req, res) => {

  let user = await UserManager.getAllUser() 
  res.send(user); 

}));

router.get('/:id', (async (req, res) => {

  let user = await UserManager.getUserById(req.params.id) 
  res.send(user); 

}));

router.delete('/:id', async (req,res) => {
  let user = await UserManager.deleteUser(req.params.id)
  res.send();
})

router.post('/getToken',validateSchema(loginUserSchema), (async (req, res) => {

    let token = await UserManager.loginUser(req.body.email, req.body.password) 
    res.send(token); 
  
  }));


module.exports = router;