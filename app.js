const express = require('express');
const db = require('./db');
const actorRouter = require('./routers/actor');
const directorRouter = require('./routers/director');
const genderRouter = require('./routers/gender');
const movieRouter = require('./routers/movie');
const userRouter = require('./routers/user');
const validateToken = require('./routers/validateToken');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

function errorHandler (err, req, res, next) {
    return res.status(500).send(err.message);
}

app.use(errorHandler); 
app.use(validateToken);

app.use("/actor", actorRouter);
app.use("/director", directorRouter);
app.use("/gender", genderRouter);
app.use("/movie", movieRouter); 
app.use("/user", userRouter); 



db.sequelize.sync().then(() => {
    app.listen(3002);
    console.log("API is running on port 3002");
});



