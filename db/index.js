const Sequelize = require('sequelize');

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'movies.db',
    // logging: false
    // global options
    define:{
      freezeTableName: true,
      timestamps: false,
    },
  
  });
  
  const db = {
    sequelize,
    Sequelize,
    models: {},
  };
  
  db.models.Actor = require('./models/actor.js')(sequelize);
  db.models.Director = require('./models/director')(sequelize);
  db.models.Gender = require('./models/gender')(sequelize);
  db.models.Movie = require('./models/movie')(sequelize);
  db.models.User= require('./models/user')(sequelize);
  
  module.exports = db;