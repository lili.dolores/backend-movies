const Sequelize = require('sequelize');

module.exports = (sequelize) => {
    class User extends Sequelize.Model {}
    User.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        },  

      email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: 'Please provide a value email"',
          },
          notEmpty: {
            msg: 'Please provide a value email',
          },
        },
      },
      
      password: Sequelize.STRING,

    }, { sequelize });
  
  
    return User;
  }; 