const Sequelize = require('sequelize');

module.exports = (sequelize) => {
    class Actor extends Sequelize.Model {}
    Actor.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        },  

      fullName: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: 'Please provide a value for "fullName"',
          },
          notEmpty: {
            msg: 'Please provide a value for "fullName"',
          },
        },
      },
      
      dateOfBirth: Sequelize.DATE,

    }, { sequelize });
  
  
    return Actor;
  }; 