const Sequelize = require('sequelize');

module.exports = (sequelize) => {
    class Movie extends Sequelize.Model {}
    Movie.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        },  
      
      codigo: Sequelize.INTEGER,

      title: Sequelize.STRING,

      year: Sequelize.DATE,

      description: Sequelize.STRING,

      stars: Sequelize.FLOAT, 

      gender_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        foreignKey: true,
        references: {
          model: 'gender',
          key: 'id'
        }
      },

      director_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        foreignKey: true,
        references: {
          model: 'director',
          key: 'id'
        }
      },

      photo_url: Sequelize.STRING,

      
    
    }, { sequelize });
  
  
    return Movie;
  }; 