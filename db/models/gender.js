const Sequelize = require('sequelize');

module.exports = (sequelize) => {
    class Gender extends Sequelize.Model {}
    Gender.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        },  

      description: Sequelize.STRING,
      
    
    }, { sequelize });
  
  
    return Gender;
  }; 